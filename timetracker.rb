#time tracker application

require "./db_connection"
require "pry"
# require "time"

class TimeTracker
  def initialize
    @design = "\n*...*.......*........*.........*........*..........*"
    @design1 = "*...*.......*........*.........*........*..........*"
    #creating database object
    @timer = 0
    @connect = Connection.new
  end

  #to add the timer with a task
  def start_timer
    print "\n\t What activity do you want to do ? \t :"
    task = gets.chomp.downcase
    print "\n\t For how long do you want to do it (estimated time) \t:"
    estimated_time = gets.chomp.downcase
    time = Time.now
    s_time = time.strftime("%Y-%m-%d %H:%M:%S")
    if task.empty?
      puts "*******Do not leave acitivy empty***********"
    else
      @connect.add_timer(task, estimated_time, s_time)
      puts "\t Successfully Added"
      puts "\t #{task.capitalize} has beend added !"
    end
  end

  #to list out the history of the time tracker
  def timer_history
    @connect.record_timer
    puts "\n \n \n"
    delete_timer
  end

  # checking the current log
  def current_log
    print "Put the name of the task to check the time log: \t"
    cur_log = gets.chomp.downcase
    records = @connect.search(cur_log)
    if records == cur_log
      @connect.present_log(records)
    else
      puts "\t Records not found !"
    end
  end

  #delete single record
  def delete_single_record
    single_record = gets.chomp.downcase
    records = @connect.search(single_record)
    # binding.pry
    if records == single_record
      puts "\t Data being deleted !! wait a minute....."
      @connect.single_record_delete(records)
      print "\n \t Record Deleted: \t #{records}"
    else
      puts "Record not found !"
    end
    timer_history
  end

  #deleteing all records
  def delete_all_record
    puts "\n \t Erasing all data now............... \t"
    @connect.all_record_delete
    puts @design
  end

  # deleting the task
  def delete_timer
    delete_record = 0
    puts @design
    puts "\t \tDelete Menu"
    puts @design1
    until delete_record == "3"
      puts "\n \t(1) Delete single task \n \t(2) Erase the history \n \t(3) Exit"
      # @connect.delete_from_history(task)
      delete_record = gets.chomp.downcase
      case delete_record

      when "1"
        print "\t Enter the task you want to delete:\t"
        delete_single_record
      when "2"
        print "\t Do you really want to erase all? (y):\t"
        choice = gets.chomp.downcase
        if choice == "y"
          delete_all_record
        else
          delete_timer
        end
      when "3"
        puts "\t Good Bye From Delete Menu"
        run_app
      else
        text_3 = "\t Provide the input as mentioned"
        puts text_3.upcase
        puts @design
      end
    end
  end

  #to update the task
  def redo_timer
    print "\n\t Please enter the task name you want to update: \t"
    old_task = gets.chomp.to_s.downcase
    records = @connect.search(old_task)
    if records == old_task
      print "\n\t Enter the task you want to assign: \t"
      new_task = gets.chomp.to_s.downcase
      @connect.update_timer(records, new_task)
      print "\n\t Task Updated: \t" + new_task
    else
      puts "\t Record is not found in our list"
    end
  end

  # to stop the time
  def stop_timer
    print "\n\t Enter the name of the task that you want to stop?\t:"
    e_task = gets.chomp.downcase
    time = Time.now
    e_time = time.strftime("%Y-%m-%d %H:%M:%S")
    records = @connect.search(e_task)
    if records == e_task
      print "\n\t Do you want to stop your timer?(y):\t"
      choice = gets.chomp.downcase
      if choice == "y"
        @connect.end_timer(records, e_time)
        text_3 = "\t Timer Stopped"
        puts text_3.upcase
        puts @design
        # else
        #   run_app
      end
    else
      puts "\t Record Not found !"
    end
  end

  # main method to run the app
  def run_app
    puts @design
    text_1 = "\t \ttime tracker application"
    puts text_1.upcase
    puts @design1

    text_2 = "\t shedule your time here"
    puts text_2.upcase
    until @timer == "q"
      puts " \n\t Select The Options Listed Below"
      puts "\t press:\n \t(1) START \n \t(2) REDO \n \t(3) CURRENT LOG \n \t(4) STOP  \n \t(5) HISTORY \n \t(q) EXIT"
      @timer = gets.chomp.downcase
      case @timer
      when "1"
        start_timer
      when "2"
        print "\n\t update your details: \t"
        redo_timer
      when "3"
        current_log
      when "4"
        stop_timer
      when "5"
        print "\n \t All records: \t"
        timer_history
      when "q"
        puts @design

        puts "\t \t Good Bye! "
        exit
      else
        text_3 = "\t Provide the input as mentioned"
        puts text_3.upcase
        puts @design
      end
    end
  end
end

obj = TimeTracker.new
obj.run_app
