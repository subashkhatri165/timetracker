# Subash Khatri
# Time Tracker CLI Application
### September 17, 2019
## Overview
As a part of the Bootcamp final milestone for Ruby, CLI Time tracker in Ruby is designed and written as per the requirements provided. The application should incorporate all previously gained knowledge including Object Oriented Principles - Classes, Objects, TDD, Exception Handling, Debugging, gems and a mysql database.

## Goals
Developing a time tracker which has full functionality of creating, deleting, updating and listing (CRUD). 
## REQUIREMENTS
By definition, Time Tracker is the application that schedules the time for our daily activities. The main purpose of this application is to manage time. For example, if a person is working in project for a time and he forgets the submission day, time tracker would be helpful to track the time for the project. 
Due to the limited time of only 3 days, lack of hardware (and ability to use) and a simple command line interface, time tracker is limited by scope and user input. Thus, using a simple tabular structure in the command line to display and interface with users and an intuitive program argument definition, a time tracker would be designed and created. Further specification defined as:
* Command line based user input (Make good use of arguments and flags to store and  * manipulate time regrading user)
* Single User System (preferably)
* Proper use of Object Oriented principles (Classes and Objects)
* Make use of rspec (TDD | testing to some degree) & pry (debugging) if needed
* Proper exception handling
* Mysql database integration
* Use of gems (pry, and similar gems)


# About the the project 
When the appication is started, a user will see a list of his/ her recent activities. It will show the duration, started time and end time of the activity with a small note on it. User will have a option of of adding new activities, deleting old actvities and also updating the existing activity.

### structure 

| id | task     | estimatedtime | start            | end_time    |

|  2 | swimming | 2 hours | 2019-09-19 21:45:51    | 2019-09-19 21:46:36 |
