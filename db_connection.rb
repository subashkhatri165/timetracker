
require "mysql2"
require "pry"

class Connection
  def initialize
    @table_design = "\tName\t Estimated \tStarted time\t \t Endtime \t "
    # puts "connection in process"
    begin
      @connect = Mysql2::Client.new(host: "localhost", username: "subash", password: "password", database: "timetrackers")
      @connect.query("
                        CREATE TABLE IF NOT EXISTS trackers (
                        id INT PRIMARY KEY AUTO_INCREMENT,
                        task VARCHAR(255) UNIQUE, 
                        estimatedtime VARCHAR(255),                    
                        start DATETIME,                   
                        duration VARCHAR(255),
                        end_time DATETIME)
                    ")
      #puts "Db Connected"
    rescue => e
      puts "Error connecting to database"
      puts e.message
    end
  end

  #adding to database
  def add_timer(task, user_time, s_time)
    begin
      @connect.query("INSERT INTO trackers (task, estimatedtime, start) VALUES('#{task}', '#{user_time}', '#{s_time}')")
      # puts "successfully Stored in db"
    rescue => e
      puts "Database Error in Adding Timer !"
      puts e.message
    end
  end

  #listing all records
  def record_timer
    puts "\n*...*.......*........*.........*........*..........*"
    puts @table_design
    begin
      results = @connect.query("SELECT * FROM trackers")

      results.to_a.each do |result|
        task_name = result["task"]
        # puts "Name of the task: #{task_name.capitalize}"

        estimated_time = result["estimatedtime"]
        # puts "Estimated time: #{estimated_time} "

        started_time = result["start"]
        # puts "Started time: #{started_time.strftime("%H:%M:%S")}"

        #todo
        # total_duration = result["duration"]
        # puts "Total Durations:" +task_duration(task_name)

        stoped_time = result["end_time"]

        if stoped_time.nil?
          puts "#{task_name.capitalize}\t\t|#{estimated_time}\t|\t#{started_time.strftime("%H:%M:%S")}\t|\t #{stoped_time} \t\t|"
        else
          puts "#{task_name.capitalize}\t\t|#{estimated_time}\t\t|#{started_time.strftime("%H:%M:%S")}\t|\t #{stoped_time.strftime("%H:%M:%S")} \t|"
        end
      end
    rescue => e
      puts "Database Error in listing data !"
      puts e.message
    end
  end

  #update timer
  def update_timer(old_data, new_data)
    begin
      @connect.query("UPDATE trackers SET task = '#{new_data}' WHERE task = '#{old_data}'")
    rescue => e
      puts "Database Error !"
      puts e.message
    end
  end

  #stopping the started time
  def end_timer(e_task, e_time)
    begin
      @connect.query("UPDATE trackers SET end_time = '#{e_time}' WHERE task = '#{e_task}'")
    rescue => e
      puts "Database Error in updating timer!"
      puts e.message
    end
  end

  #duration
  # def task_duration(e_task)
  #   p e_task

  #   s_time = @connect.query(" SELECT start FROM trackers WHERE task = '#{e_task}' ")
  #   e_time   = @connect.query("SELECT end_time FROM trackers WHERE task = '#{e_task}' ")
  #   p s_time
  #   p e_time
  #   # if e_time.nil?
  #     puts "Your timer is not stopped yet"
  #   else
  #     start_time = s_time.to_time
  #     end_time = e_time.to_time
  #     total_duration = end_time - start_time
  #   end
  #   total_duration
  # end
  #deleting single record from database
  def single_record_delete(data)
    begin
      @connect.query("DELETE FROM trackers WHERE task = '#{data}' ")
    rescue => e
      puts "Database Error !"
      puts e.message
    end
  end

  #all records from database

  def all_record_delete
    begin
      @connect.query("TRUNCATE TABLE trackers")
    rescue => exception
      puts "Database Error !"
    end
  end

  #Query For Search
  def search(by_task)
    result = ""
    begin
      records = @connect.query("SELECT task FROM trackers WHERE task = '#{by_task}'")
      records.each do |record|
        result = record["task"]
      end
    rescue => e
      puts "Database error in Search !"
      puts e.message
    end
    result
  end

  def present_log(task_log)
    puts @table_design
    begin
      results = @connect.query("SELECT * FROM trackers WHERE task = '#{task_log}'")
      results.each do |result|
        task_name = result["task"]
        estimated_time = result["estimatedtime"]
        started_time = result["start"]
        stoped_time = result["end_time"]
        if stoped_time.nil?
          puts "#{task_name.capitalize}\t|#{estimated_time}\t\t|#{started_time.strftime("%H:%M:%S")}\t| #{stoped_time} \t |"
        else
          puts "#{task_name.capitalize}\t\t|#{estimated_time}\t\t|#{started_time.strftime("%H:%M:%S")}\t|\t #{stoped_time.strftime("%H:%M:%S")} |"
        end
      end
    rescue => exception
      puts "\t Database Error in Single log !"
      puts exception.message
    end
  end
end
