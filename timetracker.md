# time tracker cli application

# Features Provided by this command-line application are:
* ### Add
* ### update
* ### view
* ### delete

# Overview of the project 
When the appication is started, a user will see a list of his/ her recent activities. It will show the duration, started time and end time of the activity with a small note on it. User will have a option of of adding new activities, deleting old actvities and also updating the existing activity.

### structure 

**Day** **start** **end** **Duration** ***Notes***
       |          |       |           |           
       |          |       |           | 
       |          |       |           |